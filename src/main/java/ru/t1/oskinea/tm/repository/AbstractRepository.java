package ru.t1.oskinea.tm.repository;

import ru.t1.oskinea.tm.api.repository.IRepository;
import ru.t1.oskinea.tm.exception.entity.EntityNotFoundException;
import ru.t1.oskinea.tm.model.AbstractModel;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    private final Map<String, M> models = new LinkedHashMap<>();

    @Override
    public M add(final M model) {
        models.put(model.getId(), model);
        return model;
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public boolean existsById(final String id) {
        return findOneById(id) != null;
    }

    @Override
    public List<M> findAll() {
        return new ArrayList<>(models.values());
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        return findAll()
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public M findOneById(final String id) {
        return models.get(id);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return models.get(index);
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Override
    public M remove(final M model) {
        if (model == null) throw new EntityNotFoundException();
        models.remove(model.getId());
        return model;
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        if (collection == null) return;
        collection.stream()
                .map(AbstractModel::getId)
                .forEach(models::remove);
    }

    @Override
    public M removeById(final String id) {
        final M model = findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

    @Override
    public M removeByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

}
