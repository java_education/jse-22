package ru.t1.oskinea.tm.command.project;

import ru.t1.oskinea.tm.api.service.IProjectService;
import ru.t1.oskinea.tm.api.service.IProjectTaskService;
import ru.t1.oskinea.tm.command.AbstractCommand;
import ru.t1.oskinea.tm.enumerated.Role;
import ru.t1.oskinea.tm.enumerated.Status;
import ru.t1.oskinea.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    protected void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
