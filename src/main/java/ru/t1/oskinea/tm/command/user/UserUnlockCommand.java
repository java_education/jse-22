package ru.t1.oskinea.tm.command.user;

import ru.t1.oskinea.tm.enumerated.Role;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "Unlock user.";

    private final String NAME = "user-unlock";

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.print("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
