package ru.t1.oskinea.tm.command.user;

import ru.t1.oskinea.tm.enumerated.Role;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "User login.";

    private final String NAME = "login";

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.print("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
