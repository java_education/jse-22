package ru.t1.oskinea.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    private static final String DESCRIPTION = "Delete all projects.";

    private static final String NAME = "project-clear";

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        final String userId = getUserId();
        getProjectService().clear(userId);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
