package ru.t1.oskinea.tm.command;

import ru.t1.oskinea.tm.api.model.ICommand;
import ru.t1.oskinea.tm.api.service.IAuthService;
import ru.t1.oskinea.tm.api.service.IServiceLocator;
import ru.t1.oskinea.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract void execute();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract String getName();

    public abstract Role[] getRoles();

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += (!result.isEmpty() ? ", " : "") + argument;
        if (description != null && !description.isEmpty()) result += (!result.isEmpty() ? " - " : "") + description;
        return result;
    }

}
