package ru.t1.oskinea.tm.command.user;

import ru.t1.oskinea.tm.api.service.IAuthService;
import ru.t1.oskinea.tm.api.service.IUserService;
import ru.t1.oskinea.tm.command.AbstractCommand;
import ru.t1.oskinea.tm.exception.entity.UserNotFoundException;
import ru.t1.oskinea.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @Override
    public String getArgument() {
        return null;
    }

}
