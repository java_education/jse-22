package ru.t1.oskinea.tm.api.service;

import ru.t1.oskinea.tm.api.repository.IUserRepository;
import ru.t1.oskinea.tm.enumerated.Role;
import ru.t1.oskinea.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByEmail(String email);

    User findByLogin(String login);

    boolean isEmailExist(String email);

    boolean isLoginExist(String login);

    void lockUserByLogin(String login);

    User removeByEmail(String email);

    User removeByLogin(String login);

    User setPassword(String id, String password);

    void unlockUserByLogin(String login);

    User updateUser(String id, String firstName, String lastName, String middleName);

}
