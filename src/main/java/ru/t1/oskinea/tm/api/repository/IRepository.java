package ru.t1.oskinea.tm.api.repository;

import ru.t1.oskinea.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<M extends AbstractModel> {

    interface IRepositoryOptional<M> {

        Optional<M> findOneById(String id);

        Optional<M> findOneByIndex(Integer index);

    }

    default IRepositoryOptional<M> optional() {
        return new IRepositoryOptional<M>() {
            @Override
            public Optional<M> findOneById(final String id) {
                return Optional.ofNullable(IRepository.this.findOneById(id));
            }

            @Override
            public Optional<M> findOneByIndex(final Integer index) {
                return Optional.ofNullable(IRepository.this.findOneByIndex(index));
            }
        };
    }

    M add(M model);

    void clear();

    boolean existsById(String id);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    int getSize();

    M remove(M model);

    void removeAll(Collection<M> collection);

    M removeById(String id);

    M removeByIndex(Integer index);

}
